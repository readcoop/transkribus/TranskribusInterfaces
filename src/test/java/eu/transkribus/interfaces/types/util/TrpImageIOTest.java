package eu.transkribus.interfaces.types.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Iterator;

import javax.imageio.spi.IIORegistry;
import javax.imageio.spi.ImageReaderSpi;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TrpImageIOTest {
	private static final Logger logger = LoggerFactory.getLogger(TrpImageIOTest.class);
	
	@Test
	public void testReadImage() throws IOException, URISyntaxException {
		TrpImageIO.listImageIOServices();		
		
		URL imgUrl = getTestImgUrl();
		
		BufferedImage biFromUrl = TrpImageIO.read(imgUrl);
		logger.info("Read buffered image from URL: {}", biFromUrl);
		final int typeFromUrl = biFromUrl.getType();
		
		File imgFile = new File(getTestImgUrl().toURI());
		
		BufferedImage biFromFile = TrpImageIO.read(imgFile);
		logger.info("Read buffered image from file: {}", biFromFile);
		final int typeFromFile = biFromFile.getType();
		
		byte[] imgBytes = Files.readAllBytes(imgFile.toPath());
		
		BufferedImage biFromBytes = TrpImageIO.read(imgBytes);
		logger.info("Read buffered image from bytes: {}", biFromBytes);
		final int typeFromBytes = biFromBytes.getType();
		
		Assert.assertEquals(typeFromUrl, typeFromFile);
		Assert.assertEquals(typeFromFile, typeFromBytes);
	}
	
	private URL getTestImgUrl() {
		return this.getClass().getClassLoader().getResource("img_orientation/view_20170929_124415.jpg");
	}
	
//	@Test
	/**
	 * This test can be used to compare concurrent readers for a specific format.
	 *    
	 * @throws IOException
	 */
	public void testReadImageReaderPlugins() throws IOException {
		String path = "/tmp/layout_test/00273.tif";
		TrpImageIO.listImageIOServices();
		BufferedImage bi = TrpImageIO.read(new File(path));
		logger.info("Read buffered image: {}", bi);
		final int type1 = bi.getType();
		
		//github.jaiimageio reader was once pulled in with planet dependencies in CITlabModule
//		final String suspectName = "com.github.jaiimageio.impl.plugins.tiff.TIFFImageReaderSpi";
		//twelvemonkes is the regular transkribus ImageIO plugin
		final String suspectName = "com.twelvemonkeys.imageio.plugins.tiff.TIFFImageReaderSpi";
		
		
		
		IIORegistry registry = IIORegistry.getDefaultInstance();
		ImageReaderSpi suspect = null;
		Iterator<ImageReaderSpi> spiIt = registry.getServiceProviders(ImageReaderSpi.class, false);
		while(spiIt.hasNext()) {
			ImageReaderSpi spi = spiIt.next();
			if(suspectName.equals(spi.getClass().getCanonicalName())) {
				suspect = spi;
				break;
			}
		}
		if(suspect == null) {
			logger.info("Suspect spi not found.");
			return;
		}
		
		boolean unregistered = registry.deregisterServiceProvider(suspect, javax.imageio.spi.ImageReaderSpi.class);
		logger.info("unregistered {} = {}", suspect.getClass().getCanonicalName(), unregistered);
		
		BufferedImage bi2 = TrpImageIO.read(new File(path));
		logger.info("Read buffered image: {}", bi2);
		final int type2 = bi2.getType();
		
		Assert.assertEquals(type2, type1);
		
//		for (String s : bi.getPropertyNames()) {
//			logger.info(s + " = " + bi.getProperty(s));
//		}
	}
}
