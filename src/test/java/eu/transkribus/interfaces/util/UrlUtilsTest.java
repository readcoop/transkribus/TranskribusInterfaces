package eu.transkribus.interfaces.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.interfaces.util.URLUtils.RemoteFile;

public class UrlUtilsTest {
	private static final Logger logger = LoggerFactory.getLogger(UrlUtilsTest.class);
	
	@Test 
	public void testUnknownHost() throws IOException {
		HttpURLConnection theGoodUrlConnection = Mockito.mock(HttpURLConnection.class);
		// random byte sequence, no real image
		byte[] expectedImageBytes = {0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x0A, 0x00, 0x0A, 0x00};
    	InputStream bais = new ByteArrayInputStream(expectedImageBytes);
		Mockito.doReturn(bais).when(theGoodUrlConnection).getInputStream();
		Mockito.doReturn(200).when(theGoodUrlConnection).getResponseCode();
		URLStreamHandler stubUrlHandler = createStub(theGoodUrlConnection);
		URL existingHost = new URL("https", "transkribus.eu", 80, "/", stubUrlHandler);
		
		HttpURLConnection theBadUrlConnection = Mockito.mock(HttpURLConnection.class);
		URLStreamHandler badStubUrlHandler = createStub(theBadUrlConnection);
		URL unknownHost = new URL("http", "www.asdlakjdalskdjl.com", 80, "/", badStubUrlHandler);
		Mockito.doThrow(new UnknownHostException(unknownHost.getHost())).when(theBadUrlConnection).connect();
		
		for(int i = 0; i < 100; i++) {
			try (InputStream is = URLUtils.getInputStream(existingHost)) {
				//success
			} catch (IOException e) {
				Assert.fail("Could not connect to valid URL: " + existingHost);
			}
			try (InputStream is = URLUtils.getInputStream(unknownHost)) {
				Assert.fail("Could connect to invalid URL: " + unknownHost);
			} catch (Exception e) {
				logger.trace(e.getClass().getName() + ": " + e.getMessage());
			}
		}
	}
	
	/**
	 * Wrap given Connection Mock in StreamHandler
	 * @param conn
	 * @return
	 */
	URLStreamHandler createStub(HttpURLConnection conn) {
		return new URLStreamHandler() {
			@Override
			protected URLConnection openConnection(URL u) throws IOException {
				return conn;
			}            
		};
	}
	
	@Test
	public void testLoadFile() {
		String[] urls = {
				"https://files.transkribus.eu/iiif/2/UNKRNHSATTZGUUMKZBSBNOUC/full/max/0/default.jpg",
				"https://files.transkribus.eu/Get?id=UNKRNHSATTZGUUMKZBSBNOUC&fileType=view"
		};
		for(String url : urls) {
			try {
				final long start = System.currentTimeMillis();
				RemoteFile file = URLUtils.loadFile(new URL(url));
				logger.debug("Successfully loaded {} in {} ms", file.getUrl(), System.currentTimeMillis() - start);
				logger.debug("Filename = {}, content length = {}", file.getFilename(), file.getData().length);
			} catch (MalformedURLException e) {
				logger.warn("Invalid URL: {}", url);
			} catch (IOException e) {
				Assert.fail("Loading remote file failed: " + e.getMessage());
			}
		}
	}
}
